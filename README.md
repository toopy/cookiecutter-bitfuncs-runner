# CookieCutter Template For BitFuncs-Runner Setup

## Generate

Generate the project tree:

```bash
$ cookiecutter gl:toopy/cookiecutter-bitfuncs-runner
... [?]: ?
```

## Install

Install files and start services

```bash
$ sudo make -C bitfuncs-runner install
# ensure dedicated user
...
```

## Gotcha!

Systemctl status:

```bash
$ sudo systemctl status bitfuncs-runner
...
```

## LICENSE

MIT
