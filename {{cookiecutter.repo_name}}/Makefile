# target: all - Default target. Does nothing.
all:
	echo "Hello $(LOGNAME), nothing to do by default"
	# sometimes: echo "Hello ${LOGNAME}, nothing to do by default"
	echo "Try 'make help'"

# target: help - Display callable targets.
help:
	egrep "^# target:" [Mm]akefile

# target: install - Install files and enable services
install:
	# ensure dedicated user
	egrep -i "^bitfuncs" /etc/group  || groupadd -g {{cookiecutter.bitfuncs_gid}} bitfuncs
	egrep -i "^bitfuncs" /etc/passwd || useradd  -g {{cookiecutter.bitfuncs_gid}} -m -u {{cookiecutter.bitfuncs_uid}} bitfuncs
	usermod -aG docker bitfuncs
	# ensure config files
	cp config/* /home/bitfuncs/
	chown bitfuncs:bitfuncs /home/bitfuncs/*
	# ensure volume dir
	mkdir -p {{cookiecutter.docker_volumes_dir}}
	cp /home/bitfuncs/bitfuncs.yml {{cookiecutter.docker_volumes_dir}}	|| true
	chown -R bitfuncs:bitfuncs {{cookiecutter.docker_volumes_dir}}
	# build docker image
	DOCKER_BUILDKIT=1 docker build \
		--build-arg GID={{cookiecutter.bitfuncs_gid}} \
		--build-arg UID={{cookiecutter.bitfuncs_uid}} \
		--tag=bitfuncs-runner \
		--target=runner \
		docker
	# ensure docker-compose for bitfuncs
	sudo -iu bitfuncs {{cookiecutter.pip_command}} install --user docker-compose
	# copy start script
	mkdir -p /usr/local/sbin
	cp sbin/bitfuncs-runner /usr/local/sbin
	# setup runner service
	cp systemd/bitfuncs-runner.* /etc/systemd/system
	systemctl daemon-reload
	systemctl enable bitfuncs-runner.timer
	systemctl start bitfuncs-runner.timer

# target: uninstall - Disable services and uninstall files
uninstall:
	# clean runner timer
	systemctl stop bitfuncs-runner.timer         || true
	systemctl disable bitfuncs-runner.timer      || true
	rm -rf /etc/systemd/system/bitfuncs-runner.*
	# clean bitfuncs-runner script
	rm /usr/local/sbin/bitfuncs-runner           || true
	# down containers
	sudo -iu bitfuncs {{cookiecutter.docker_compose_command}} down --remove-orphans -t0